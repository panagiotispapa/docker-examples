#!/usr/bin/env bash

docker run -it --rm --name create-my-app -v "$PWD":/usr/src/app -w /usr/src/app demo/node_angular:8 ng new ang-app