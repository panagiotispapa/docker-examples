#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml

function cleanup() {
    docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} rm -fsv serve-app
}

trap cleanup EXIT

docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} up serve-app

