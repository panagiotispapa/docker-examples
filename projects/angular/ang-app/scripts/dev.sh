#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

docker run -it --rm --name ang-app-dev -p 4200:4200 -v "$PROJECT_DIR":/usr/src/app -w /usr/src/app demo/node_angular:8 /bin/bash
