import axios from 'axios';

class ToDosApi {
    static getToDos(callback) {
        axios.get("https://jsonplaceholder.typicode.com/todos").then(callback);
    }
    static createToDos(title, userId, callback, errorCallback) {
        axios.post("https://jsonplaceholder.typicode.com/todos", {
            title: title,
            userId: userId,
            completed: false
        }).then(callback).catch(errorCallback);
    }

}

export default ToDosApi