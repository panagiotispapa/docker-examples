import axios from 'axios';

class UsersApi {
    static getUsers(callback) {
        axios.get("https://jsonplaceholder.typicode.com/users").then(callback);
    }

    static getUser(userId, callback) {
        axios.get("https://jsonplaceholder.typicode.com/users/" + userId).then(callback);
    }
}

export default UsersApi