// http://calapi.inadiutorium.cz/api/v0/en/calendars/general-en/today
import axios from 'axios';

class CalendarAPI  {

    static getServerUrl() {
        let serverUrl = process.env.REACT_APP_SERVER_URL;
        console.log("++++++++ REACT_APP_SERVER_URL: " + serverUrl);
        return serverUrl || "";
    }
    static getCelebrationsForToday(callback) {
        axios.get(this.getServerUrl() + "/api/v0/en/calendars/general-en/today").then(callback);
    }
}

export default CalendarAPI