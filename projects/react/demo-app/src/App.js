import React, {Component} from 'react';
// import logo from './logo.svg';

import Users from "./components/Users";
import ToDos from "./components/ToDos";

import {BrowserRouter as Router, Route} from 'react-router-dom';
import User from "./components/User";
import TodayCelebrations from "./components/TodayCelebrations";

class App extends Component {

    render() {
        return (
            <Router>
                <div className="App">
                    <Route exact path="/" component={Users}/>
                    <Route exact path="/users" component={Users}/>
                    <Route exact path="/users/:userId" component={User}/>
                    <Route path="/todos" component={ToDos}/>
                    <Route path="/celebrations" component={TodayCelebrations}/>
                </div>

            </Router>

        );
    }
}

export default App;
