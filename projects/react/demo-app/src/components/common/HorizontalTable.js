import React, {Component} from 'react';
import Table from "./Table";
import PropTypes from 'prop-types';

class HorizontalTable extends Component {

    render() {
        return (
            <Table header={this.props.header || []} body={[this.props.data]} condensed={this.props.condensed}
                   striped={this.props.striped} bordered={this.props.bordered}/>
        )
    }

}

HorizontalTable.propTypes = {
    header: PropTypes.array,
    data: PropTypes.array,
    condensed: PropTypes.bool,
    bordered: PropTypes.bool,
    striped: PropTypes.bool
};

export default HorizontalTable