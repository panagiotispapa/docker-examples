import React, {Component} from 'react';
import Table from "./Table";
import PropTypes from 'prop-types';

class VerticalTable extends Component {

    render() {
        return (
            <Table header={[]} body={this.props.data.map(el => [el])} condensed={this.props.condensed} striped={this.props.striped}/>
        )
    }
}

VerticalTable.propTypes = {
    data: PropTypes.array,
    condensed: PropTypes.bool,
    bordered: PropTypes.bool,
    striped: PropTypes.bool
};

export default VerticalTable