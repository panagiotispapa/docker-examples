import React, {Component} from 'react';
import { Table as BSTable}  from 'react-bootstrap';
import PropTypes from 'prop-types';

class Table extends Component {
    render() {
        return (
            <BSTable responsive={true} condensed={this.props.condensed}
                     striped={this.props.striped}
                     bordered={this.props.bordered}
            >
                <TableHeader data={this.props.header}/>
                <TableBody data={this.props.body}/>
            </BSTable>
        )
    }
}

const TableHeader = props => {
    // console.log(props);
    const headers = props.data.map((row, index) => {
            return (
                <th key={index}>{row}</th>
            )
        }
    );

    return (
        <thead>
        <tr>
            {headers}
        </tr>
        </thead>
    );
};

const TableBody = props => {
    // console.log(props);
    const tds = columns => columns.map((r, i) => {
            return (
                <td key={i}>{r}</td>
            );
        }
    );

    const trs = props.data.map((r, i) => {
            return (
                <tr key={i}>{tds(r)}</tr>
            );
        }
    );

    return (
        <tbody>
        {trs}
        </tbody>
    );
};

Table.propTypes = {
    header: PropTypes.array,
    body: PropTypes.array,
    condensed: PropTypes.bool,
    bordered: PropTypes.bool,
    striped: PropTypes.bool
};


export default Table