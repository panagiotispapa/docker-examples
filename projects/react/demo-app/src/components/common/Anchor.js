import React, {Component} from 'react';
import {Glyphicon} from 'react-bootstrap';
import PropTypes from 'prop-types';

class Anchor extends Component {

    render() {
        return (
            <a
                href={this.props.href}
                {...(this.props.openInNewTab && {target: "_blank"})}
                {...(this.props.shouldLookLikeAButton && {role: "button", className: "btn btn-default btn-lg"})}
                {...(this.props.tooltip && {'data-toggle': "tooltip", 'title': this.props.tooltip})}
            >

                {this.props.text}
                {this.props.glyphClass && <Glyphicon glyph={this.props.glyphClass} />}
            </a>
        );
    }
}

Anchor.propTypes = {
    href: PropTypes.string.isRequired,
    text: PropTypes.string,

    openInNewTab: PropTypes.bool,
    shouldLookLikeAButton: PropTypes.bool,
    bordered: PropTypes.bool,
    glyphClass: PropTypes.string,
    tooltip: PropTypes.string,
};


export default Anchor