import React, {Component} from 'react';
import ToDosApi from "../api/ToDosApi";
import TopNavBar from "./TopNavBar";
import CreateTodoButton from "./CreateTodoButton";
import CreateTodoModal from "./modals/CreateTodoModal";
import CreatedTodoModal from "./modals/CreatedTodoModal";
import ToDosTable from "./ToDosTable";

class ToDos extends Component {

    constructor(props) {
        super(props);

        this.state = {
            toDos: [],
            showCreateTodoModal: false,
            showCreatedTodoModal: false,
            createdTodoData: {}
        };
    }

    componentDidMount() {
        // const values = queryString.parse(this.props.location.search);

        ToDosApi.getToDos(res => {
                const toDos = res.data;
                this.setState({toDos: toDos});
                // console.log(users);
            }
        );
    }

    render() {
        let toDos = this.state.toDos;
        // console.log(toDos);
        return (
            <div>
                <TopNavBar/>
                <CreateTodoButton setParentState={p => this.setState(p)}/>
                <CreateTodoModal setParentState={p => this.setState(p)} show={this.state.showCreateTodoModal}/>
                <CreatedTodoModal setParentState={p => this.setState(p)} show={this.state.showCreatedTodoModal}
                                  data={this.state.createdTodoData}/>
                <h1>{toDos.length + " ToDos"}</h1>
                <ToDosTable toDos={toDos} />
            </div>

        )
    }
}

export default ToDos