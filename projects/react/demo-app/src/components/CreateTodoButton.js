import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import PropTypes from "prop-types";

class CreateTodoButton extends Component {

    constructor(props, context) {
        super(props, context);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.setParentState({
            showCreateTodoModal: true
        });
    }

    render() {
        return (
            <Button bsStyle="primary" onClick={this.handleClick}>
                Create
            </Button>
        )
    }
}

CreateTodoButton.propTypes = {
    setParentState: PropTypes.func.isRequired
};
export default CreateTodoButton