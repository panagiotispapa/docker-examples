import React, {Component} from 'react';
import CalendarAPI from "../api/CalendarAPI";
import Table from "./common/Table";

class TodayCelebrations extends Component {

    constructor(props) {
        super(props);

        this.state = {
            response: {
                date: "",
                rank: "",
                celebrations: [],
            }
        };
    }

    componentDidMount() {

        CalendarAPI.getCelebrationsForToday(res => {
                const response = res.data;
                this.setState({response: response});
                // console.log(users);
            }
        );
    }

    render() {
        const response = this.state.response;
        return (
            <div>
                <h1>{response.date}</h1>
                <h2>Week {response.season_week}</h2>
                <Table header={["Title", "Rank"]} body={response.celebrations.map(c => [
                    c.title,
                    c.rank
                ])} striped={true}/>
            </div>
        )
    }
}


export default TodayCelebrations