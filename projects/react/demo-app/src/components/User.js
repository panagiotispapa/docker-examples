import React, {Component} from 'react';
import UsersApi from "../api/UsersApi";
import UsersTable from "./UsersTable";

class User extends Component {

    constructor(props) {
        super(props);

        this.state = {
            users: []
        };
    }

    componentDidMount() {
        // const values = queryString.parse(this.props.location.search);
        console.log("props " + JSON.stringify(this.props));
        // console.log("userId: " + this.props.match.params.userId);

        // console.log("values" + values.);
        const userId = this.props.match.params.userId;
        UsersApi.getUser(userId, res => {
                const user = res.data;
                this.setState({users: [user]});
                // console.log(users);
            }
        );
    }

    render() {
        return (
            <div>
                <UsersTable users={this.state.users}/>

            </div>
        )
    }
}


export default User