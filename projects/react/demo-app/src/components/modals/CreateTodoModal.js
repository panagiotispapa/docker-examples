import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Modal} from 'react-bootstrap';
import TextGroup from "./TextGroup";
import ToDosApi from "../../api/ToDosApi";

class CreateTodoModal extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleEnter = this.handleEnter.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.onUserIdChange = this.onUserIdChange.bind(this);

        this.state = {
            todoTitle:"",
            todoUserId:""
        };
    }

    handleEnter() {

    }

    handleClose() {
        this.props.setParentState({showCreateTodoModal: false});
    }

    handleCreate() {
        this.props.setParentState({showCreateTodoModal: false});
        // this.props.setParentState({jenkinsJobUrl: "http://google.com"});
        // this.props.setParentState({showOpenJenkinsModal: true});

        // console.log("todoTitle: " + this.state.todoTitle);
        // console.log("todoUserId: " + this.state.todoUserId);

        ToDosApi.createToDos(this.state.todoTitle, this.state.todoUserId, res => {
            console.log("res: " + JSON.stringify(res));
            this.props.setParentState({
                showCreatedTodoModal: true,
                createdTodoData: res.data
            });
        }, error => {
            console.log("Error happened: " + error);
        })
    }

    onTitleChange(evt) {
        this.setState({
            todoTitle:evt.target.value
        });

    }

    onUserIdChange(evt) {
        this.setState({
            todoUserId:evt.target.value
        });

    }

    render() {
        return (
            <div>
                <Modal show={this.props.show} onEnter={this.handleEnter}>
                    <Modal.Header>
                        <Modal.Title>Create a todo</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form>
                            <TextGroup id={"create-todo-title"} label="Title:" value={""} onChange={this.onTitleChange}
                            />
                            <TextGroup id={"create-todo-userId"} label="UserId:" value={""} onChange={this.onUserIdChange}
                                       />
                            {/*<TextGroup id={"build-branch-id"} label="Branch:" value={this.props.branch}*/}
                            {/*readOnly={"readOnly"}/>*/}

                        </form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Close</Button>
                        <Button bsStyle="primary" onClick={this.handleCreate}>Create</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }

}

CreateTodoModal.propTypes = {
    setParentState: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
};

export default CreateTodoModal