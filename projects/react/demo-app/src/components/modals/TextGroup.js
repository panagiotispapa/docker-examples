import React, {Component} from 'react';
import {ControlLabel, FormControl, FormGroup} from 'react-bootstrap';
import PropTypes from 'prop-types';

class TextGroup extends Component{
    render() {
        const props = this.props;
        return (
            <FormGroup controlId={props.id}>
                <ControlLabel>{props.label}</ControlLabel>
                <FormControl type="text" defaultValue={props.value}
                             {...(props.readOnly && {readOnly: props.readOnly})}
                             {...(props.onChange && {onChange: props.onChange})}
                />
            </FormGroup>
        )
    }
}

TextGroup.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func,
    readOnly: PropTypes.string
};

export default TextGroup