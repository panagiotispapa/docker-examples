import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Modal} from 'react-bootstrap';
import TextGroup from "./TextGroup";

class CreatedTodoModal extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleEnter = this.handleEnter.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            todoTitle: "",
            todoUserId: ""
        };
    }

    handleEnter() {

    }

    handleClose() {
        this.props.setParentState({showCreatedTodoModal: false});
    }

    render() {

        const data = this.props.data;
        return (
            <div>
                <Modal show={this.props.show} onEnter={this.handleEnter}>
                    <Modal.Header>
                        <Modal.Title>Created a todo</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form>
                            <TextGroup id={"created-todo-title"} label="Title:" value={data.title}
                                       readOnly={"readOnly"}/>
                            <TextGroup id={"created-todo-userId"} label="UserId:" value={data.userId}
                                       readOnly={"readOnly"}/>
                            <TextGroup id={"created-todo-completed"} label="Completed:" value={data.completed}
                                       readOnly={"readOnly"}/>
                        </form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Close</Button>

                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

CreatedTodoModal.propTypes = {
    setParentState: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired

};

export default CreatedTodoModal