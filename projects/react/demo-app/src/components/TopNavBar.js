import React, {Component} from 'react';
import {MenuItem, Nav, Navbar, NavDropdown} from 'react-bootstrap';

class TopNavBar extends Component {
    render() {
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/">Home</a>
                    </Navbar.Brand>
                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        {/*<NavItem eventKey={1} href="#">*/}
                        {/*Link*/}
                        {/*</NavItem>*/}
                        {/*<NavItem eventKey={2} href="#">*/}
                        {/*Link*/}
                        {/*</NavItem>*/}
                        <NavDropdown eventKey={1} title="Pages" id="basic-nav-dropdown">
                            <MenuItem eventKey={1.1} href={"/users"}>Users</MenuItem>
                            <MenuItem eventKey={1.2} href={"/todos"}>To Dos</MenuItem>
                            <MenuItem eventKey={1.3} href={"/celebrations"}>Celebrations</MenuItem>

                        </NavDropdown>
                    </Nav>
                    {/*<Nav pullRight>*/}
                    {/*<NavItem eventKey={1} href="#">*/}
                    {/*Link Right*/}
                    {/*</NavItem>*/}
                    {/*<NavItem eventKey={2} href="#">*/}
                    {/*Link Right*/}
                    {/*</NavItem>*/}
                    {/*</Nav>*/}
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default TopNavBar