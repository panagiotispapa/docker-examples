import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Table from "./common/Table";

class ToDosTable extends Component {

    render() {
        const toDos = this.props.toDos || [];
        return (
            <Table
                striped={true}
                header={["id", "userId", "title", "completed"]}

                body={toDos.map(toDo => [
                    toDo.id,
                    toDo.userId,
                    toDo.title,
                    toDo.completed + ""
                ])}
            />
        )

    }
}

ToDosTable.propTypes = {
    toDos: PropTypes.array.isRequired,
};

export default ToDosTable