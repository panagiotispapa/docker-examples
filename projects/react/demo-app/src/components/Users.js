import React, {Component} from 'react';
import UsersApi from "../api/UsersApi";
import TopNavBar from "./TopNavBar";
import UsersTable from "./UsersTable";

class Users extends Component {

    constructor(props) {
        super(props);

        this.state = {
            users: []
        };
    }

    componentDidMount() {

        UsersApi.getUsers(res => {
                const users = res.data;
                this.setState({users: users});
                // console.log(users);
            }
        );
    }

    render() {
        return (
            <div>
                <TopNavBar/>
                <h1>Users</h1>
                <UsersTable users={this.state.users}/>


            </div>
        )
    }
}


export default Users