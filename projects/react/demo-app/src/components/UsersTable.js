import React, {Component} from 'react';
import PropTypes from 'prop-types';
import HorizontalTable from "./common/HorizontalTable";
import Table from "./common/Table";
import Anchor from "./common/Anchor";
import VerticalTable from "./common/VerticalTable";

class UsersTable extends Component {
    render() {
        return (
            <Table
                header={["Name", "email", "phone", "Address", "Company"]}
                body={this.props.users.map(user => [
                    <Anchor href={"users/" + user.id} text={user.name} openInNewTab={true}/>,
                    // user.name,
                    user.email,
                    user.phone,
                    <Address address={user.address}/>,
                    <Company company={user.company}/>

                ])}
            />
        )
    }
}

class Address extends Component {
    render() {
        const header = ["street", "suite", "city", "zipcode"];
        return (
            <HorizontalTable header={header} data={header.map(h => this.props.address[h])}/>
        )
    }
}

class Company extends Component {
    render() {
        const header = ["name", "info"];
        let company = this.props.company;
        return (
            <HorizontalTable header={header} data={[
                company.name,
                <VerticalTable data={[company.catchPhrase, company.bs]}/>
            ]}/>
        )
    }
}

Address.propTypes = {
    address: PropTypes.object.isRequired,
};

Company.propTypes = {
    company: PropTypes.object.isRequired,
};

UsersTable.propTypes = {
    users: PropTypes.array.isRequired,
};

export default UsersTable