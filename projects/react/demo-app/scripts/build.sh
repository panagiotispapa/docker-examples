#!/usr/bin/env bash

function calculate_tags {
    export GIT_TAG=$(git tag -l --points-at HEAD)
    export COMMIT_HASH=$(git rev-parse HEAD | cut -c1-11)
    export IMAGE_TAG=${IMAGE_TAG:-${GIT_TAG:-"${COMMIT_HASH}"}}
    export TAG=${IMAGE_TAG}
}


export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml


calculate_tags

docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} build demo-react-app