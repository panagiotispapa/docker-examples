#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml

docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} down -v