#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

(rm -rf ${PROJECT_DIR}/build)
(rm -rf ${PROJECT_DIR}/node_modules)