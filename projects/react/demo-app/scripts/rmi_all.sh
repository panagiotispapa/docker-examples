#!/usr/bin/env bash

function remove_images {
        echo "Image $1"
        docker images -q "demo/$1"  | xargs docker rmi -f
}

remove_images demo-react-app