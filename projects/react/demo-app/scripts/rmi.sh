#!/usr/bin/env bash

function calculate_tags {
    export GIT_TAG=$(git tag -l --points-at HEAD)
    export COMMIT_HASH=$(git rev-parse HEAD | cut -c1-11)
    export IMAGE_TAG=${IMAGE_TAG:-${GIT_TAG:-"${COMMIT_HASH}"}}
    export TAG=${IMAGE_TAG}
}

calculate_tags

echo "Removing demo/demo-react-app:${TAG}"
docker rmi demo/demo-react-app:${TAG}