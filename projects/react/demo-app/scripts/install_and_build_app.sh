#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

docker run --rm -v "$PROJECT_DIR":/usr/src/app -w /usr/src/app demo/node_react:8 npm i
docker run --rm -v "$PROJECT_DIR":/usr/src/app -w /usr/src/app demo/node_react:8 npm run build
