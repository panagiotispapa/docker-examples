#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

docker run -it --rm --name demo-react-app-dev -p 3000:3000 -v "$PROJECT_DIR":/usr/src/app -w /usr/src/app -e REACT_APP_SERVER_URL="http://calapi.inadiutorium.cz" demo/node_react:8 /bin/bash