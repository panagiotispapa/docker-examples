#!/usr/bin/env bash

function wait_for_service {
    SERVICE_HOST=$1 SERVICE_PORT=$2 docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} up dockerize
}

function cleanup() {
    docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} rm -fsv dockerize
}

trap cleanup EXIT

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml
export TAG=${TAG:-$(git rev-parse HEAD | cut -c1-11)}


docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} up -d demo-react-app

wait_for_service run-app 80