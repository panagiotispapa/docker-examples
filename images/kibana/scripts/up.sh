#!/usr/bin/env bash

function wait_for_service {
    SERVICE_HOST=$1 SERVICE_PORT=$2 docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} up dockerize
}

function cleanup() {
    docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} rm -fsv dockerize
}

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml

trap cleanup EXIT

docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} up -d kibana

wait_for_service kibana 5601