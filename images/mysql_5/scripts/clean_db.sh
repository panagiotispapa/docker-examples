#!/usr/bin/env bash

function cleanup {
    docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} rm -fsv demo-db-clean
}

trap cleanup EXIT

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml

docker-compose -f ${DOCKER_COMPOSE_FILE} up demo-db-clean