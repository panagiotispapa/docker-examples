#!/usr/bin/env bash

function remove_images {
        echo "Service $1"
        docker images -q "demo/$1"  | xargs docker rmi -f
}

remove_images demo-db-migration