#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

(${PROJECT_DIR}/scripts/./clean_db.sh && ${PROJECT_DIR}/scripts/./migrate_using_local.sh)
