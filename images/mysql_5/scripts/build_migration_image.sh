#!/usr/bin/env bash

function build {
    echo "Building"
    docker build -t ${IMAGE_TAG} -f ${PROJECT_DIR}/Dockerfile-db-migration ${PROJECT_DIR}
}

function ask_user_for_building_if_image_exists {
    echo "Image already exists for version $IMAGE_TAG. Do you want to build again?"
    select yn in "yes" "no"; do
        case $yn in
            yes ) export BUILD_IF_IMAGE_EXISTS='true'; break;;
            no ) export BUILD_IF_IMAGE_EXISTS='false'; break;;
        esac
    done
}

service_name="demo-db-migration"
hash=$(git rev-parse HEAD | cut -c1-11)
export IMAGE_TAG="demo/${service_name}:${hash}"
images_found=$(docker images ${IMAGE_TAG} | wc -l)
#images_found=0

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

if [ ${images_found} -gt 1 ]; then
    [ -z "$BUILD_IF_IMAGE_EXISTS" ] && ask_user_for_building_if_image_exists
    if [ ${BUILD_IF_IMAGE_EXISTS} = 'true' ]; then
        build
    else
        echo ""
    fi
else
    build
fi