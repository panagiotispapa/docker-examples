#!/usr/bin/env bash

function create_volume {
    docker volume ls -q -f "name=$1" | grep -q . || docker volume create --name=$1
}

create_volume demo-db-data