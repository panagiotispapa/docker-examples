#!/usr/bin/env bash

function wait_for_service {
    SERVICE_HOST=$1 SERVICE_PORT=$2 docker-compose --log-level ERROR -f ${DOCKER_COMPOSE_FILE} up dockerize
}

function start_service_and_wait_until_healthy {
    docker-compose up -d $1
    wait_until_service_is_up $1
}

function wait_until_service_is_up {
    container_id=$1
    echo "Waiting for $1"
    while [ $(docker inspect --format "{{json .State.Health.Status }}" "$container_id") != "\"healthy\"" ];
        do printf ".";
        sleep 1;
    done

    echo ""
}

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml

start_service_and_wait_until_healthy rabbit-mq
