#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export MONGO_DB_TAG="4.0.3-xenial"

docker run -it --rm --network=demo -v ${PROJECT_DIR}/seed:/seed mongo:${MONGO_DB_TAG} \
mongoimport --host mongo -u root -p pass --authenticationDatabase admin \
--db local --collection demo --type json --file /seed/init.json --jsonArray

