#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}
export MONGO_DB_TAG="4.0.3-xenial"

docker-compose --log-level ERROR -f ${WORKSPACE}/docker-compose.yml down -v
