#!/usr/bin/env bash

function logs_for_service {
    container_id=$1
    docker logs -f ${container_id}
}

function ask_user_for_service {
    echo "For which service do you want to see logs?"
    select yn in "mongo" "mongo-express"; do
        case $yn in
            mongo ) export SERVICE_ID=mongo; break;;
            mongo-express ) export SERVICE_ID=mongo-express; break;;
        esac
    done
}

ask_user_for_service
logs_for_service ${SERVICE_ID}


