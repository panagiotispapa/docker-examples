#!/usr/bin/env bash

export MONGO_DB_TAG="4.0.3-xenial"

docker run -it --rm --network=demo mongo:${MONGO_DB_TAG} mongo --host mongo -u root -p pass --authenticationDatabase admin test
