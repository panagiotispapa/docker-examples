#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

export hash=$(git rev-parse HEAD | cut -c1-11)

export TAG=${hash}
export DOCKER_COMPOSE_FILE=${PROJECT_DIR}/docker-compose.yml
export MONGO_DB_TAG="4.0.3-xenial"

docker-compose -f ${DOCKER_COMPOSE_FILE} up -d mongo
docker-compose -f ${DOCKER_COMPOSE_FILE} up -d mongo-express