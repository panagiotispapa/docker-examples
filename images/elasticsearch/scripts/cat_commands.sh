#!/usr/bin/env bash

export ES_SERVER=${ES_SERVER:-'http://localhost:9200'}

function ask_user_for_command {
    echo "Choose command"
    select cmd in "nodes" "indices" "aliases" "allocation" "health" "master" "repositories" "shards" "segments"; do
        case $cmd in
            nodes ) export CAT_CMD='nodes'; break;;
            indices ) export CAT_CMD='indices'; break;;
            aliases ) export CAT_CMD='aliases'; break;;
            allocation ) export CAT_CMD='allocation'; break;;
            health ) export CAT_CMD='health'; break;;
            master ) export CAT_CMD='master'; break;;
            repositories ) export CAT_CMD='repositories'; break;;
            shards ) export CAT_CMD='shards'; break;;
            segments ) export CAT_CMD='segments'; break;;
        esac
    done
}

ask_user_for_command

curl ${ES_SERVER}/_cat/${CAT_CMD}?v