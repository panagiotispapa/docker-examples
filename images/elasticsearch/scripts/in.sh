#!/usr/bin/env bash

function open_bash_for_container {
    container_id=$1
    docker exec -it ${container_id} /bin/bash
}

open_bash_for_container elasticsearch